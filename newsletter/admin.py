from django.contrib import admin

from .models import Newsletter, Client, Message


# Register your models here.

@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('id', 'start', 'finish', 'message_text', 'tag_filter')


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone_number', 'operator_code', 'timezone', 'tag')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_newsletter_id', 'status', 'get_client_phone_number')

    def get_newsletter_id(self, obj):
        return obj.newsletter_id.id

    def get_client_phone_number(self, obj):
        return obj.client_id.phone_number

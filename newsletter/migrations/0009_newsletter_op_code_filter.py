# Generated by Django 4.0.2 on 2022-02-25 12:37

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0008_alter_newsletter_tag_filter'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsletter',
            name='op_code_filter',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=20), blank=True, null=True, size=10),
        ),
    ]

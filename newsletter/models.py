import pytz
from django.contrib.postgres.fields import ArrayField
from django.db import models

from core.models import TimeStampMixin


# Create your models here.

class Newsletter(TimeStampMixin):
    start = models.DateTimeField(auto_now=False, auto_now_add=False)
    finish = models.DateTimeField(auto_now=False, auto_now_add=False)
    message_text = models.TextField()
    tag_filter = ArrayField(models.CharField(max_length=20), size=10, null=True, blank=True)
    op_code_filter = ArrayField(models.CharField(max_length=20), size=10, null=True, blank=True)


class Client(TimeStampMixin):
    phone_number = models.CharField(max_length=13)
    tag = models.CharField(max_length=20)

    TIMEZONES = tuple(zip(pytz.common_timezones, pytz.common_timezones))

    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    @property
    def operator_code(self):
        return self.phone_number[3:5]

    def __str__(self):
        return self.phone_number


class Message(TimeStampMixin):
    status = models.BooleanField(default=False)
    newsletter_id = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.newsletter_id.id, self.client_id.phone_number)

from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, viewsets
from rest_framework.response import Response

from .functions import schedule_newsletter
from .models import Client, Newsletter
from .serializers import ClientSerializer, ClientDataWithOpCodeSerializer, NewsletterSerializer


# Create your views here.

@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_description="Create Clients"
))
class ClientViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet,
                    mixins.DestroyModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def get_serializer_class(self):
        if self.action in ['retrieve', 'list']:
            return ClientDataWithOpCodeSerializer
        return ClientSerializer


class NewsletterViewSet(viewsets.ModelViewSet):
    serializer_class = NewsletterSerializer
    queryset = Newsletter.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)


        newsletter_id = serializer.data.get('id')
        finish_time = datetime.fromisoformat(serializer.data['finish'])
        scheduler = BackgroundScheduler()
        scheduler.add_job(func=schedule_newsletter,
                          run_date=datetime.fromisoformat(serializer.data['start']),
                          args=[newsletter_id, finish_time],
                          id=str(newsletter_id)
                          )
        scheduler.start()
        return Response({"message": "newsletter successfully created"})

import json
import time
from datetime import datetime

import requests

from .models import Newsletter, Client, Message

SMS_SERVICE_TOKEN='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzY5NjQxMzAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkFraG1hZGpvblNoYW1zdWRkaW5vdiJ9.oMuRf53aLXHdZ-kuu1D0B1os-LEQZ7szmG7rMrzmDPk'

def send_sms(message_id, phone_number, message_text, token):
    request_body = json.dumps({
        "id": message_id,
        "phone": int(phone_number),
        "text": message_text
    })
    headers = {
        'Authorization': "Bearer " + token,
    }
    url = "https://probe.fbrq.cloud/v1/send/{}".format(message_id)
    send_request = requests.post(url, data=request_body, headers=headers)
    if send_request.status_code == 200:
        print('success')
        return True
    else:
        print('fail')
        return False


def schedule_newsletter(newsletter_id, finish_time):
    newsletter = Newsletter.objects.get(id=newsletter_id)
    tags = newsletter.tag_filter
    op_codes = newsletter.op_code_filter
    sms_service_token = SMS_SERVICE_TOKEN
    if tags:
        clients = Client.objects.filter(tag__in=tags)
    else:
        clients = Client.objects.all()
    if op_codes:
        clients = [x for x in clients if x.operator_code in op_codes]
    clients = list(clients)
    for client in clients:
        message = Message.objects.create(newsletter_id=newsletter, client_id=client)
        if datetime.now(finish_time.tzinfo) >= finish_time:
            print('process_finished')
            continue
        send_message = send_sms(message.id, client.phone_number, newsletter.message_text, sms_service_token)
        if send_message:
            message.status = True
            message.save()
